
class BattleshipGame
  attr_reader :board, :player

  def initialize(player, board)
    @player = player
    @board = board
  end

  def attack(pos)
    if @board[pos] == :s
      puts "Hit!"
    else
      puts "Miss"
    end
    @board[pos] = :x
  end

  def count
    board.count
  end

  def game_over?
    board.won?
  end

  def play_turn
    self.attack(player.get_play)
  end
end
