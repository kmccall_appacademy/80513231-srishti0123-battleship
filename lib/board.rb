class Board
  attr_accessor :grid

  def initialize(grid = Board::default_grid)
    @grid = grid
  end

  def self.default_grid
    grid = Array.new(10) {Array.new(10) }
  end

  def count
    counter = 0
    @grid.flatten.each do |spot|
      counter += 1 if spot == :s
    end
    counter
  end

  def empty?(pos = nil)
    if pos
      return true if @grid[pos[0]][pos[1]] == nil
      false
    else
      @grid.flatten.all? {|pos| pos == nil }
    end
  end

  def full?
    @grid.flatten.all? {|pos| pos != nil}
  end

  def place_random_ship
    if self.full?
      raise "board is fulll"
    else
      pos = get_random_sample
      @grid[pos[0]][pos[1]] = :s
    end
  end

  def get_random_sample
    empty_res = []
    @grid.each_index do |rowidx|
      @grid[rowidx].each_index do |spotidx|
        if grid[rowidx][spotidx] == nil
          empty_res << [rowidx, spotidx]
        end
      end
    end
    empty_res.sample
  end

  def won?
    @grid.flatten.none? {|pos| pos == :s}
  end

  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def []=(pos, val)
    row, col = pos
    grid[row][col] = val
  end

end




# require 'byebug'
#
# class Board
#   attr_accessor :grid
#
#   def self.default_grid
#      Array.new(10) {Array.new(10)}
#   end
#
#   def initialize(grid = Board::default_grid)
#     @grid = grid
#   end
#
#   def count
#     grid.flatten.count {|ship| ship == :s}
#   end
#
#   def empty?(pos = nil)
#     if pos
#       return true if grid[pos[0]][pos[1]] == nil
#       false
#     else
#       return true if grid.flatten.none? {|el| el == :s} && pos == nil
#       false
#     end
#   end
#
#   def full?
#     grid.flatten.none? {|el| el == nil}
#   end
#
#   def place_random_ship
#     if self.full?
#       raise "board is fulll"
#     else
#       pos = rando_pos
#       @grid[pos[0]][pos[1]] = :s
#     end
#   end
# # debugger
#   def rando_pos
#     [rand(grid.length), rand(grid.length)]
#   end
#
#   def won?
#     grid.flatten.none? {|el| el == :s}
#   end
# end
